export declare type sizeOptions = 'small' | 'medium' | 'large' | 'xlarge';
export declare const sizes: Record<sizeOptions, string>;
export declare const sizeMap: Record<string, sizeOptions>;
